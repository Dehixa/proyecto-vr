﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentralCollect : MonoBehaviour {
	//AudioSource collectSound;
	public GameObject myObject;
	public void OnTriggerEnter(Collider other){
		//collectSound.Play();	
		Collect.theScore += 50;
		Destroy (gameObject);
		myObject.SetActive (true);
	}
}
