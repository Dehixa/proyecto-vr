﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowAndDestroy : MonoBehaviour {

	public float lifeTime =10f;

	// Update is called once per frame
	void Update () {
		if (this.gameObject.activeSelf) {
			if (lifeTime > 0) {
				lifeTime -= Time.deltaTime;
				if (lifeTime <= 0) {
					Destroy (this.gameObject);
				}
			}
		}
	}
}
